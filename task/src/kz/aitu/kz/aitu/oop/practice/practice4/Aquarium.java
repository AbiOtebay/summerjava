package kz.aitu.kz.aitu.oop.practice.practice4;

package kz.aitu.oop.practice.practice4;

class Aquarium {
    private int fish;
    private int  reptiles;
    private int accessories;

    public Aquarium(int fish,int reptiles,int accessories){
        this.fish=fish;
        this.reptiles=reptiles;
        this.accessories=accessories;

    }

    public void setFish(int fish) {
        this.fish = fish;
    }

    public void setReptiles(int reptiles) {
        this.reptiles = reptiles;
    }

    public void setAccessories(int accessories) {
        this.accessories = accessories;
    }

    public int getFish() {
        return fish;
    }

    public int getReptiles() {
        return reptiles;
    }

    public int getAccessories() {
        return accessories;
    }


}

class Count extends Aquarium{

    public Count(int fish, int reptiles, int accessories) {
        super(fish, reptiles, accessories);
    }
    public void fish_rep_acc(){
        System.out.println("----------------------------------------");
        System.out.println("Fish-5$ | Reptiles-13$  | Accessories-2$");
        System.out.println("----------------------------------------");

    }

    @Override
    public int getFish() {
        return super.getFish()*5;

    }

    @Override
    public int getReptiles() {
        return super.getReptiles()*2;
    }

    @Override
    public int getAccessories() {
        return super.getAccessories()*2;
    }

    public int All_price(){
        return getFish()+getAccessories()+getReptiles();
    }

    public void Count_fish_rep_acc(){
        System.out.println("Fish:"+getFish()+"$");
        System.out.println("Reptiles:"+getReptiles()+"$");
        System.out.println("Accessories:"+getAccessories()+"$");
        System.out.println("All price: "+All_price()+"$");
        System.out.println(" ");
    }



}



class Main{
    public static void main(String[] args) {
        Count count=new Count(20,25,10);
        count.fish_rep_acc();
        count.Count_fish_rep_acc();

        count.setFish(45);
        count.setAccessories(23);

        count.fish_rep_acc();
        count.Count_fish_rep_acc();



    }

}
