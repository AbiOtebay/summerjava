package kz.aitu.examples;

public class MyString {
    private int[] values;

    public MyString(int[] values){
        this.values=values;
    }

    public int length(){
        return values.length;
    }

    public int valueAt(int position){
        if(position < 0 || position >= length()) return -1;
        return values[position];
    }

    public boolean contains(int value){
        boolean cont=false;
        for (int i=0; i<length();i++){
            if(valueAt(i)==value){
                cont=true;
                break;
            }
        }
        if(cont==true) return true;
        else return false;
    }
    public int count(int value){
        int count=0;
        for(int i=0;i<length(); i++){
            if(valueAt(i)==value){
                count++;
            }
            return count;
        }
    }
}
